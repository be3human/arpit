<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()

     {

        parent::__construct();

    	$this->load->model('My_Model');    

        $this->load->library('csvimport');
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');

	 }


	public function index()
	{
		
		if(isset($_POST['submit']))
		{
			

        $config['upload_path'] = './uploads/';

        $config['allowed_types'] = 'csv';

        $config['max_size'] = '100000';

        $this->load->library('upload', $config);
		
		
		$session_array = array('userid'=>True);
		$this->session->set_userdata($session_array);



        // If upload failed, display error

        if (!$this->upload->do_upload('csv')) {
			

             $errors = $this->upload->display_errors();
				
             $this->session->set_flashdata('error',$errors);

             redirect(base_url(''));

        } else {

             $file_data = $this->upload->data();

            $file_path =  './uploads/'.$file_data['file_name'];

           

                

               

                   $sucssfull_update = 0;

                    $wrong_entries = 0;

                    $total_unsuccessfull = 0;

             

                    ///////////////////////////////////////////////////////////////////////

                   

                      $file = fopen($file_path, 'r');

                      $mCount = 0;

                      

                      $date = date("Y-m-d H:i:s");

                        while (($row = fgetcsv($file)) !== FALSE) {
							

                            

                            if($mCount != 0) {

                                if(!empty($row[0])){
									
                                     $user_id = trim($row[0]);

                                     $days =  trim($row[1]);
									
									 $month = trim($row[2]);
									
									 $year =  trim($row[3]);
									 
									 $basic =  trim($row[4]);
									 
									 $hra =  trim($row[5]);
									 
									 $da =  trim($row[6]);
									
									 $m_no =  date('m',strtotime($month));
									
									 $d=cal_days_in_month(CAL_GREGORIAN,$m_no,$year);
										
										
									//calculation of basic hra and da
									
									$basic_cal = ($basic * $days / $d);
									$Hra_cal = ($hra * $days / $d);
								    $da_cal = ($da * $days / $d);

				
									//Insert array start
										 
										 
								$insert_log_array = array(
			
								'user_id'=>$user_id,
			
								'basic'=>$basic_cal,
			
								'HRA'=>$Hra_cal,
			
								'DA'=>$da_cal,
								
								'month'=>$month,
								
								'year'=>$year,
			
								'uploaded_on'=>$date
			
							);
							
							 $this->My_Model->insert_entry('salary',$insert_log_array);
				
							$sucssfull_update++; 
                                                                



                                 } else {
								
                                    $wrong_entries++;
						
                                }

                             }



                           $mCount++;  

                        }

                        fclose($file);   

                  //////////////////////////////////////////////////////////////////////////////  
				

                unlink($file_path);

                $summary = "<b>Summary : </b><br><b>Total Updates</b> : $sucssfull_update";

                

                $this->session->set_flashdata('success', 'Stock list has been Updated succesfully <br>'.$summary);

                redirect(base_url());

                //echo "<pre>"; print_r($insert_data);

            

        }

     	
		
	}

  else
	{
	
		$this->load->view('welcome_message');
	}
 }
}
